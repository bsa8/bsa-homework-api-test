import { expect } from "chai";
describe("Some controller", () => {
    it('test 1', async () => {
        console.log("it is a test #1");
    });
    it('test 2', async () => {
        console.log("it is a test #2");
        expect(true).to.be.equal(true);
    });
    it('test 3', async () => {
        console.log("it is a test #3");
        expect(3).to.be.equal(3);
    });
})